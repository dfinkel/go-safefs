package safefs

import (
	"bytes"
	"errors"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"testing"
)

func TestOpen(t *testing.T) {
	tmpdir := t.TempDir()
	fsh, openErr := OpenDirFS(tmpdir)
	if openErr != nil {
		t.Fatalf("failed to open tmpdir: %s", openErr)
	}
	t.Cleanup(func() { fsh.(io.Closer).Close() })
	t.Run("nonexistent", func(t *testing.T) {
		f, nonExistentErr := fsh.Open("nonexistent")
		if nonExistentErr == nil {
			t.Errorf("successfully opened a non-existent file: (fd: %+v)", f)
		}
		if !errors.Is(nonExistentErr, fs.ErrNotExist) {
			t.Errorf("failed to unwrap error to expected fs.ErrNotExist; got: %s", nonExistentErr)
		}
	})
	t.Run("fizzle", func(t *testing.T) {
		fizzlefile := filepath.Join(tmpdir, "fizzle")
		const fizzleConts = "zizzle"
		if fizzleWrErr := os.WriteFile(fizzlefile, []byte(fizzleConts), 0500); fizzleWrErr != nil {
			t.Fatalf("failed to populate fizzle in tmpdir (%q): %s", fizzlefile, fizzleWrErr)
		}

		f, fizzleOpenErr := fsh.Open("fizzle")
		if fizzleOpenErr != nil {
			t.Fatalf("failed to open fizzle: %s", fizzleOpenErr)
		}
		defer f.Close()
		fizzleReadConts, fizzleReadErr := io.ReadAll(f)
		if fizzleReadErr != nil {
			t.Fatalf("failed to read fizzle: %s", fizzleReadErr)
		}
		if !bytes.Equal([]byte(fizzleConts), fizzleReadConts) {
			t.Errorf("unexpected contents for fizzle: %q; expected %q",
				string(fizzleReadConts), fizzleConts)
		}
	})
	t.Run("traverse_above", func(t *testing.T) {
		// this will fail because it fails fs.ValidPath (due to the `..` component)
		f, nonExistentErr := fsh.Open("../nonexistent")
		if nonExistentErr == nil {
			t.Errorf("successfully opened a non-existent file: (fd: %+v)", f)
		}
		if !errors.Is(nonExistentErr, fs.ErrInvalid) {
			t.Errorf("failed to unwrap error to expected fs.ErrInvalid; got: %s", nonExistentErr)
		}
	})
	t.Run("foobar_traverse_down", func(t *testing.T) {
		fizzlefile := filepath.Join(tmpdir, "foobar")
		const fizzleConts = "zizzle"
		if fizzleWrErr := os.WriteFile(fizzlefile, []byte(fizzleConts), 0500); fizzleWrErr != nil {
			t.Fatalf("failed to populate foobar in tmpdir (%q): %s", fizzlefile, fizzleWrErr)
		}

		if koolMkdirErr := os.Mkdir(filepath.Join(tmpdir, "kool"), 0500); koolMkdirErr != nil {
			t.Fatalf("failed to setup kool directory: %s", koolMkdirErr)
		}

		f, fizzleOpenErr := fsh.Open("kool/../foobar")
		if fizzleOpenErr == nil {
			t.Errorf("successfully opened a non-existent file: (fd: %+v)", f)
		}
		if !errors.Is(fizzleOpenErr, fs.ErrInvalid) {
			t.Errorf("failed to unwrap error to expected fs.ErrInvalid; got: %s", fizzleOpenErr)
		}
	})
	t.Run("foobar_traverse_subdir", func(t *testing.T) {
		kooldir := filepath.Join(tmpdir, "koolio")
		fizzlefile := filepath.Join(kooldir, "foobar")
		const fizzleConts = "zizzle"
		if koolMkdirErr := os.Mkdir(kooldir, 0700); koolMkdirErr != nil {
			t.Fatalf("failed to setup kool directory: %s", koolMkdirErr)
		}
		if fizzleWrErr := os.WriteFile(fizzlefile, []byte(fizzleConts), 0500); fizzleWrErr != nil {
			t.Fatalf("failed to populate foobar in tmpdir (%q): %s", fizzlefile, fizzleWrErr)
		}

		f, fizzleOpenErr := fsh.Open("koolio/foobar")
		if fizzleOpenErr != nil {
			t.Fatalf("failed to open foobar: %s", fizzleOpenErr)
		}
		defer f.Close()
		fizzleReadConts, fizzleReadErr := io.ReadAll(f)
		if fizzleReadErr != nil {
			t.Fatalf("failed to read foobar: %s", fizzleReadErr)
		}
		if !bytes.Equal([]byte(fizzleConts), fizzleReadConts) {
			t.Errorf("unexpected contents for foobar: %q; expected %q",
				string(fizzleReadConts), fizzleConts)
		}
	})
}
