//go:build linux

package safefs

import (
	"fmt"
	"io/fs"
	"os"
	"path"
	"runtime"
	"sync/atomic"
	"time"

	"golang.org/x/sys/unix"
)

func openDirFS(path string) (*dirFDFS, error) {
	fd, openErr := unix.Open(path, unix.O_PATH|unix.O_RDONLY|unix.O_CLOEXEC|unix.O_DIRECTORY, 0)
	if openErr != nil {
		return nil, fmt.Errorf("failed to open path %q: %w", path, openErr)
	}
	fs := &dirFDFS{
		dirFD:    fd,
		origPath: path,
	}

	runtime.SetFinalizer(fs, closeFD)

	return fs, nil
}

func closeFD(d *dirFDFS) error {
	// only close the fd once (lets us keep the finalizer, but also lets diligent folks use Close() for reliable closing
	if !atomic.CompareAndSwapInt32(&d.unclosed, 0, 1) {
		return nil
	}
	return unix.Close(d.dirFD)
}

type dirFDFS struct {
	dirFD    int
	origPath string
	unclosed int32
}

func (d *dirFDFS) Close() error {
	return closeFD(d)
}

func getFDRLimits() (uint64, uint64, error) {
	res := unix.Rlimit{}
	err := unix.Getrlimit(unix.RLIMIT_NOFILE, &res)
	if err != nil {
		return 0, 0, err
	}
	return res.Cur, res.Max, nil
}

// openAttempt attempts to open a file, and return an appropriate error in the case of a failure
// The second return value indicates whether retrying is appropriate.
// first return value is an FD
func (d *dirFDFS) openAttempt(name string, flags uint64) (int, bool, error) {
	how := unix.OpenHow{
		Flags: flags,
		Mode:  0,
		// Use RESOLVE_BENEATH
		Resolve: unix.RESOLVE_BENEATH,
	}
	fFD, fopenErr := unix.Openat2(d.dirFD, name, &how)
	if fopenErr == nil {
		return fFD, false, nil
	}

	e, isErrno := fopenErr.(unix.Errno)
	if !isErrno {
		return 0, false, fmt.Errorf("failed to open %q: %w", name, fopenErr)
	}
	switch e {
	case unix.EACCES, unix.EPERM:
		return 0, false, fmt.Errorf("permission denied (%d) %s opening %q: %w", e, e, name, fs.ErrPermission)
	case unix.ENOENT:
		return 0, false, fmt.Errorf("invalid path (ENOENT - %d): %q: %w", e, name, fs.ErrNotExist)
	case unix.ENOTDIR:
		return 0, false, fmt.Errorf("at least one path component is not a directory (ENOTDIR - %d): %q: %w",
			e, name, fs.ErrNotExist)
	case unix.ENFILE:
		return 0, false, fmt.Errorf("system-wide file-descriptor limit reached (ENFILE - %d): %w", e, e)
	case unix.EMFILE:
		softL, hardL, limErr := getFDRLimits()
		if limErr != nil {
			return 0, false, fmt.Errorf("file-descriptor limit reached (EMFILE - %d): %w", e, e)
		}

		return 0, false, fmt.Errorf("file-descriptor limit reached (EMFILE - %d) (limits: %d soft; %d hard): %w",
			softL, hardL, e, e)
	case unix.ELOOP:
		return 0, false, fmt.Errorf("filesystem loop encountered for path %q (%d): %w", name, e, e)
	case unix.EAGAIN:
		return 0, true, fmt.Errorf("transient error encountered (%d - EAGAIN): %w", e, e)
	case unix.EINTR:
		return 0, true, fmt.Errorf("transient error encountered (%d - EINTR): %w", e, e)
	default:
		return 0, false, fmt.Errorf("failed to open %q: %w", name, e)
	}
}

// Open opens the named file.
//
// When Open returns an error, it should be of type *PathError
// with the Op field set to "open", the Path field set to name,
// and the Err field describing the problem.
//
// Open should reject attempts to open names that do not satisfy
// ValidPath(name), returning a *PathError with Err set to
// ErrInvalid or ErrNotExist.
func (d *dirFDFS) Open(name string) (fs.File, error) {
	if !fs.ValidPath(name) {
		return nil, &fs.PathError{
			Op:   "open",
			Path: name,
			Err:  fs.ErrInvalid,
		}
	}
	fh, shouldRetry, openErr := d.openAttempt(name, unix.O_RDONLY|unix.O_CLOEXEC)
	if !shouldRetry {
		if openErr == nil {
			return os.NewFile(uintptr(fh), name), nil
		}
		return nil, &fs.PathError{
			Op:   "open",
			Path: name,
			Err:  openErr,
		}
	}
	const maxRetries = 15
	for attempt := 0; attempt < maxRetries; attempt++ {
		fh, shouldRetry, openErr = d.openAttempt(name, unix.O_RDONLY|unix.O_CLOEXEC)
		if !shouldRetry {
			if openErr == nil {
				return os.NewFile(uintptr(fh), name), nil
			}
			return nil, &fs.PathError{
				Op:   "open",
				Path: name,
				Err:  openErr,
			}
		}
		// TODO: use exponential backoff here
		time.Sleep(time.Nanosecond * 500)
	}
	return nil, fmt.Errorf("too many failures: %w", openErr)
}

// Sub returns an FS corresponding to the subtree rooted at dir.
func (d *dirFDFS) Sub(dir string) (fs.FS, error) {
	if !fs.ValidPath(dir) {
		return nil, &fs.PathError{
			Op:   "sub",
			Path: dir,
			Err:  fs.ErrInvalid,
		}
	}
	fh, shouldRetry, openErr := d.openAttempt(dir, unix.O_PATH|unix.O_DIRECTORY|unix.O_CLOEXEC)
	if openErr == nil {
		return &dirFDFS{
			dirFD:    fh,
			origPath: path.Join(d.origPath, dir),
		}, nil
	}
	if !shouldRetry {
		return nil, openErr
	}
	const maxRetries = 15
	for attempt := 0; attempt < maxRetries; attempt++ {
		fh, shouldRetry, openErr = d.openAttempt(dir, unix.O_PATH|unix.O_DIRECTORY|unix.O_CLOEXEC)
		if openErr == nil {
			return &dirFDFS{
				dirFD:    fh,
				origPath: path.Join(d.origPath, dir),
			}, nil
		}
		if !shouldRetry {
			return nil, openErr
		}
		// TODO: use exponential backoff here
		time.Sleep(time.Nanosecond * 500)
	}
	return nil, fmt.Errorf("too many failures: %w", openErr)
}
