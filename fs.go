package safefs // import "golang.spin-2.net/safefs"

import "io/fs"

// OpenDirFS creates a new fs.FS
func OpenDirFS(path string) (fs.FS, error) {
	// Delegate to a platform-specific function
	return openDirFS(path)
}
